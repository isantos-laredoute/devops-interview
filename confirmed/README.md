# Devops Confirmed Interview

The objective of that practical interview is to validate your skills to Confirmed level.

<br>

> Use more than one terminal to do the job

<br>

# Install and configure Kubernetes (using Kind)


Create the cluster with HA - High Availability and describe the architecture

<br>

<details><summary>Expectation</summary>
<p>

#### Result: 
- Docker UP and Running (Ex.: docker ps)
- Kubernetes cluster with HA

#### Expected understanding/skills of the candidate:

### Docker:

- Undertanding Docker daemon
- Undertanding Docker principals:
- - Permissions
- - Hardware
- - cgroups 
- Volume mount
- Network mode
  - Network namespace
  - IPAM config:
    - gateway
    - address
    - IP range
- Port Bindings 

</p>
<p>

- Kubernetes HA - High Availability UP and Running

#### Expected understanding/skills of the candidate:

### Kubernetes:

- control plane
  - kube-apiserver
  - kube-controller-manager
  - kube-schedule
- Workers (nodes)
  - kubelet
  - kube-proxy
- etcd



</p>
</details>
<br>

# Install Vault from Helm Chart

Using the official Helm Chart of the Vault, we want to you install the Vault on the Kubernetes environment.

<details><summary>Expectation</summary>
<p>

#### Result: 
- Install Vault over Helm Chart
- Create the Kubernetes objects:
  -  namespace, 
  -  pvc, 
  -  pv, 
  -  POD, 
  -  deploy and 
  -  service
- Vault:
  - Inicialization
  - Key/Value
  - Role
  - Policy

#### Expected understanding/skills of the candidate:

- Kubernetes Deployment process (CKAD)
- Kubernetes basic interation with API (kubectl)
- Undertanding about Vault features and integration with Kubernetes

</p>
</details>
<br>


# ETCD (etcdctl)

Using the ETCDCTL get the Vault ClusterIP.

<br>
<details><summary>Expectation</summary>
<p>

#### Result: 
- Understanding the Key/value infraestructure to store Kubernetes information

#### Expected understanding/skills of the candidate:

### Kubernetes:

- control plane
  - kube-apiserver
  - kube-controller-manager
  - kube-schedule
- etcd

</p>
</details>
<br>


# Create App

Create a simple app (using any language) to listen on 8080 port and reply the answers from that practical interview, and deploy the application on Kubernetes using the best practices deployment over Terraform:

- PodSecurityPolicy
- NetworkPolicy
- Role / RoleBinding
- Deployment
- Service
- Ingress (optional)

We want to you create the app with the handler will answer the interview questions, like:

http://app:xxx/vault = Show the commands to install the Vault on Kubernetes

http://app:xxx/etcd = Shows etcd commands and IP (plain text), it's not necessary to connect to etcd programmatically

http://app:xxx/...

http://app:xxx/...

http://app:xxx/...

<br>

> Don't forgot, use Terraform to deploy that application

<br>
<details><summary>Expectation</summary>
<p>


#### Result: 
- Understanding the logical programming
- Undertanding the Kubernetes principals and best practices
- Understanding the principals of Docker

#### Expected understanding/skills of the candidate:

- Build Docker image
- Kubernetes best practices  (CKA/CKAD)
- Undertanding about nodes and your relation with docker containers
- Undertanding about deployment object inside Kubernetes

</p>
</details>
<br>

# Create Ingress object using cURL

Create Ingress Object to your simple app using cURL command.

<br>
<details><summary>Expectation</summary>
<p>

#### Result: 
- Ingress object created on the Kubernetes environment

#### Expected understanding/skills of the candidate:

- Kubernetes Deployment process (CKAD)
- Kubernetes interation with API (kubectl)

</p>
</details>
<br>

# Scale App

Scale your application using HPA and test then.

<br>
<details><summary>Expectation</summary>
<p>

#### Result: 
- App UP and Running with HPA enabled
- Load test to application and see the HPA working

#### Expected understanding/skills of the candidate:

- Kubernetes Deployment process (CKAD)
- Kubernetes scale app HPA (CKAD)
- Kubernetes interation with API (kubectl)
- Understanding app scale principals

</p>
</details>
<br>

# Install Vault Controller

Install Vault Controller and integrate this with the Vault. 
Create a Vault policy with the Vault Controller

Use the project:

`https://github.com/melphos/vault-controller`

We'll build, deploy and create a policy from Vault Controller

<br>
<details><summary>Expectation</summary>
<p>

#### Result: 
- Vault Controller integrated with the Vault
- Create a Policy from CRD
- 

#### Expected understanding/skills of the candidate:

- Kubernetes Deployment process (CKAD)
- Kubernetes interation with API (kubectl)
- Understanding Vault
- Understanding Kubernetes Operators (controller)
- Understanding Go language
- Understanding build and deploy process

</p>
</details>
<br>

# Create Pipeline

Create a pipeline to build and deploy (terraform) your app

<br>
<details><summary>Expectation</summary>
<p>

#### Result: 
- Pipeline with steps:
  - build
  - deploy
  - create policy on Vault with Vault Controller
- Create a Policy from CRD


#### Expected understanding/skills of the candidate:

- Principals of the app lifecycle environment (pipeline)

</p>
</details>
<br>

# Deploy KaaS - Kubernetes as a Service

Create a Kubernetes as a Service on any cloud provider.

You can use Terraform or SDK from the Cloud provider.

Not necessary show to us the infrastructure created on the Cloud, only the steps to create the infrastructure.

<br>
<details><summary>Expectation</summary>
<p>

#### Result: 
- Configure VPC
- Defined Subnet:
  - POD
  - Service
  - Nodepool
- Type of nodepool
- Cloud IAM permission
- Kubernetes deploy
- Route scheme to Kubernetes
- Firewall rules to access the Kubernetes

#### Expected understanding/skills of the candidate:

- Principals of the Cloud environment

</p>
</details>
<br>