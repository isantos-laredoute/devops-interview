# Devops Senior Interview

The objective of that practical interview is to validate your skills to Senior level.

> Use more than one terminal to do the job

# Design a Kubernetes production ready

Create a simple diagram to show to us whats you need to go to production ready on the Kubernetes environment using KaaS - Kubernetes as a Service (GKE, EKS, AKS, etc) using Terraform and Helm.

<br>

<details><summary>Expectation</summary>
<p>

#### Result: 
- A list of the recommeded Kubernetes deploy architecture
- A list of the basic apps to support the production ready on the Kubernetes environment, ex.:
  - Vault
  - Prometheus
  - EFK
  - Consul
  - Istio
  - etc

#### Expected understanding/skills of the candidate:

- Undertanding Kubernetes architecture
- Undertanding the principals to put a application on production ready
- Understanding the cloud environment

</p>
<p>




</p>
</details>
<br>

# Create a Kubernetes Operators

Create a simple kubernetes operators and explain the steps to up and running.

Create the pipeline to deploy this on the envrironment.

<br>

<details><summary>Expectation</summary>
<p>

#### Result: 
- Kubernetes Operator deploy and running

#### Expected understanding/skills of the candidate:

- Undertanding Kubernetes architecture
- Undertanding the principals to put a application on production ready
- Understanding the Operators on Kubernetes
- Understanding logical programming
- Understanding pipeline best practices

</p>
<p>

</p>
</details>
<br>